<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 20.4.15
 * Time: 9:01
 */

namespace Podhy\GopayBundle\Controller;


use Adwell\Shop\OrdersBundle\Entity\Order;
use Adwell\Shop\OrdersBundle\Entity\PaymentDetails\Gopay;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Podhy\GopayBundle\Order\OrderRepositoryInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * @Route(service="podhy_gopay_controller_notify")
 */
class NotifyController
{
    private $goId;

    private $secureKey;

    private $successUrl;

    private $failureUrl;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var OrderRepositoryInterface
     */
    private $repository;

    public function __construct($goId, $secureKey, $successUrl, $failureUrl, Router $router, Registry $doctrine, Logger $logger, OrderRepositoryInterface $repository)
    {
        $this->goId = $goId;
        $this->secureKey = $secureKey;
        $this->successUrl = $successUrl;
        $this->failureUrl = $failureUrl;
        $this->router = $router;
        $this->doctrine = $doctrine;
        $this->logger = $logger;
        $this->repository = $repository;
    }

    /**
     * @Route("/_podhy_gopay/callback", name="podhy_gopay_callback")
     * @Template()
     */
    public function callbackAction(Request $request)
    {
        $returnedPaymentSessionId = $request->query->get('paymentSessionId');
        $returnedGoId = $request->query->get('targetGoId');
        $returnedOrderNumber = $request->query->get('orderNumber');
        $returnedEncryptedSignature = $request->query->get('encryptedSignature');

        $this->logger->info('Online platba IP: ' . $request->getClientIp());
        $this->logger->info('Online platba URL: ' . $request->getUri());

        /** @var Gopay $payment */
        $payment = $this->repository->findOneByPaymentSessionId($returnedPaymentSessionId);
        if ($payment)
        {
            try
            {
                \GopayHelper::checkPaymentIdentity(
                    (float)$returnedGoId,
                    (float)$returnedPaymentSessionId,
                    null,
                    $returnedOrderNumber,
                    $returnedEncryptedSignature,
                    (float)$this->goId,
                    $payment->getOrderNumber(),
                    $this->secureKey);

                $result = \GopaySoap::isPaymentDone(
                    (float)$returnedPaymentSessionId,
                    (float)$this->goId,
                    $payment->getOrderNumber(),
                    (int)$payment->getTotalPriceInCents(),
                    $payment->getCurrency(),
                    $payment->getProductName(),
                    $this->secureKey);

                $successUrl = new RedirectResponse($this->router->generate($this->successUrl, [], UrlGenerator::ABSOLUTE_URL));
                $failureUrl = new RedirectResponse($this->router->generate($this->failureUrl, [], UrlGenerator::ABSOLUTE_URL));

                if ($result["sessionState"] == \GopayHelper::PAID ) {
                    /*
                     * Zpracovat pouze objednavku, ktera jeste nebyla zaplacena
                     */
                    if ($payment->getOrder()->getState() != Order::STATE_ZAPLACENA) {

                        /*
                         *  Zpracovani objednavky  ! UPRAVTE !
                         */
                        $payment->getOrder()->setState(Order::STATE_ZAPLACENA);
                        $this->doctrine->getManager()->flush();
                    }

                    /*
                     * Presmerovani na prezentaci uspesne platby
                     */
                    return $successUrl;

                } else if ( $result["sessionState"] == \GopayHelper::PAYMENT_METHOD_CHOSEN) {
                    /* Platba ceka na zaplaceni */
                    $payment->getOrder()->setState(Order::STATE_CEKAJICI);
                    $this->doctrine->getManager()->flush();
                    return $successUrl;

                } else if ( $result["sessionState"] == \GopayHelper::CREATED) {
                    /* Platba nebyla zaplacena */
                    return $failureUrl;
                } else if ( $result["sessionState"] == \GopayHelper::CANCELED) {
                    /* Platba byla zrusena objednavajicim */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return $failureUrl;

                } else if ( $result["sessionState"] == \GopayHelper::TIMEOUTED) {
                    /* Platnost platby vyprsela  */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return $failureUrl;

                } else if ( $result["sessionState"] == \GopayHelper::AUTHORIZED) {
                    /* Platba byla autorizovana, ceka se na dokonceni  */
                    $payment->getOrder()->setState(Order::STATE_CEKAJICI);
                    $this->doctrine->getManager()->flush();
                    return $successUrl;

                } else if ( $result["sessionState"] == \GopayHelper::REFUNDED) {
                    /* Platba byla vracena - refundovana  */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return $successUrl;
                } else {
                    /* Chyba ve stavu platby */
                    $result["sessionState"] = \GopayHelper::FAILED;
                    return $failureUrl;
                }
            }
            catch (\Exception $e)
            {

            }
        }
    }

    /**
     * @Route("/_podhy_gopay/notify", name="podhy_gopay_notify")
     */
    public function notifyAction(Request $request)
    {
        $returnedPaymentSessionId = $request->query->get('paymentSessionId');
        $returnedParentPaymentSessionId = $request->query->get('parentPaymentSessionId');
        $returnedGoId = $request->query->get('targetGoId');
        $returnedOrderNumber = $request->query->get('orderNumber');
        $returnedEncryptedSignature = $request->query->get('encryptedSignature');

        $this->logger->info('Online platba IP: ' . $request->getClientIp());
        $this->logger->info('Online platba URL: ' . $request->getUri());

        /** @var Gopay $payment */
        $payment = $this->repository->findOneByPaymentSessionId($returnedPaymentSessionId);
        if ($payment)
        {
            try
            {
                \GopayHelper::checkPaymentIdentity(
                    (float)$returnedGoId,
                    (float)$returnedPaymentSessionId,
                    null,
                    $returnedOrderNumber,
                    $returnedEncryptedSignature,
                    (float)$this->goId,
                    $payment->getOrderNumber(),
                    $this->secureKey);

                $result = \GopaySoap::isPaymentDone(
                    (float)$returnedPaymentSessionId,
                    (float)$this->goId,
                    $payment->getOrderNumber(),
                    (int)$payment->getTotalPriceInCents(),
                    $payment->getCurrency(),
                    $payment->getProductName(),
                    $this->secureKey);

                if ($result["sessionState"] == \GopayHelper::PAID) {
                    /*
                     * Zpracovat pouze objednavku, ktera jeste nebyla zaplacena
                     */
                    if ($payment->getOrder()->getState() != Order::STATE_ZAPLACENA) {

                        /*
                         *  Zpracovani objednavky  ! UPRAVTE !
                         */
                        $payment->getOrder()->setState(Order::STATE_ZAPLACENA);
                        $this->doctrine->getManager()->flush();
                    }

                    return new Response();

                } else if ( $result["sessionState"] == \GopayHelper::CANCELED) {
                    /* Platba byla zrusena objednavajicim */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return new Response();

                } else if ( $result["sessionState"] == \GopayHelper::TIMEOUTED) {
                    /* Platnost platby vyprsela  */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return new Response();

                } else if ( $result["sessionState"] == GopayHelper::REFUNDED) {
                    /* Platba byla vracena - refundovana */
                    $payment->getOrder()->setState(Order::STATE_ZRUSENA);
                    $this->doctrine->getManager()->flush();
                    return new Response();

                } else if ( $result["sessionState"] == GopayHelper::AUTHORIZED) {
                    /* Platba byla autorizovana, ceka se na dokonceni  */
                    $payment->getOrder()->setState(Order::STATE_CEKAJICI);
                    $this->doctrine->getManager()->flush();
                    return new Response();

                } else {
                    return new Response('', 500);
                }
            }
            catch (\Exception $e)
            {
                return new Response('', 500);
            }
        }
    }
} 