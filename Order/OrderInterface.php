<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 17.4.15
 * Time: 13:07
 */

namespace Podhy\GopayBundle\Order;


interface OrderInterface
{
    public function getPaymentSessionId();
    public function setPaymentSessionId($paymentSessionId);
    public function getProductName();
    public function getTotalPriceInCents();
    public function getCurrency();
    public function getOrderNumber();
    public function getFirstName();
    public function getLastName();
    public function getCity();
    public function getStreet();
    public function getPostalCode();
    public function getCountryCode();
    public function getEmail();
    public function getPhoneNumber();
} 