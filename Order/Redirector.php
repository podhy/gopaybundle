<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.4.15
 * Time: 11:32
 */

namespace Podhy\GopayBundle\Order;


use Symfony\Component\HttpFoundation\RedirectResponse;

class Redirector
{
    private $goId;

    private $secureKey;

    public function __construct($goId, $secureKey)
    {
        $this->goId = $goId;
        $this->secureKey = $secureKey;
    }

    /**
     * @return RedirectResponse
     */
    public function redirect(OrderInterface $order)
    {
        $encryptedSignature = \GopayHelper::encrypt(
            \GopayHelper::hash(
                \GopayHelper::concatPaymentSession((float)$this->goId,
                    (float)$order->getPaymentSessionId(),
                    $this->secureKey)
            ), $this->secureKey);

        return new RedirectResponse(
            \GopayConfig::fullIntegrationURL()
            . '?sessionInfo.targetGoId=' . $this->goId
            . '&sessionInfo.paymentSessionId=' . $order->getPaymentSessionId()
            . '&sessionInfo.encryptedSignature=' . $encryptedSignature
        );
    }
}