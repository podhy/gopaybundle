<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 20.4.15
 * Time: 13:01
 */

namespace Podhy\GopayBundle\Order;


interface OrderRepositoryInterface
{
    public function findOneByPaymentSessionId($paymentSessionId);
} 