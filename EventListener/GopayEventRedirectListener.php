<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 18.4.15
 * Time: 13:04
 */

namespace Podhy\GopayBundle\EventListener;


use Adwell\Shop\OrdersBundle\Event\OrderConfirmEvent;
use Podhy\GopayBundle\Order\OrderInterface;
use Podhy\GopayBundle\Order\Redirector;

class GopayEventRedirectListener
{
    private $redirector;

    function __construct(Redirector $redirector)
    {
        $this->redirector = $redirector;
    }

    public function onOrderConfirmEvent(OrderConfirmEvent $event)
    {
        if ($event->getOrder()->getPaymentDetails() instanceof OrderInterface)
        {
            $event->setResponse($this->redirector->redirect($event->getOrder()->getPaymentDetails()));
            $event->stopPropagation();
        }
    }
} 