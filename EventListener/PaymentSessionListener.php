<?php
/**
 * Created by PhpStorm.
 * User: podhor01
 * Date: 17.4.15
 * Time: 12:36
 */

namespace Podhy\GopayBundle\EventListener;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Podhy\GopayBundle\Order\OrderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;

class PaymentSessionListener
{
    private $goId;

    private $successUrl;

    private $failedUrl;

    private $secureKey;

    private $lang;

    /**
     * @var Router
     */
    private $router;

    public function __construct($goId, $successUrl, $failedUrl, $secureKey, $lang, Router $router)
    {
        $this->goId = $goId;
        $this->successUrl = $successUrl;
        $this->failedUrl = $failedUrl;
        $this->secureKey = $secureKey;
        $this->lang = $lang;
        $this->router = $router;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof OrderInterface)
        {
            $callbackUrl = $this->router->generate('podhy_gopay_callback', [], UrlGeneratorInterface::ABSOLUTE_URL);

            $paymentSessionId = \GopaySoap::createPayment(
                (float)$this->goId,
                $entity->getProductName(),
                (int)$entity->getTotalPriceInCents(),
                $entity->getCurrency(),
                $entity->getOrderNumber(),
                $callbackUrl,
                $callbackUrl,
                [],
                'eu_om',
                $this->secureKey,
                $entity->getFirstName(),
                $entity->getLastName(),
                $entity->getCity(),
                $entity->getStreet(),
                $entity->getPostalCode(),
                $entity->getCountryCode(),
                $entity->getEmail(),
                $entity->getPhoneNumber(),
                null,
                null,
                null,
                null,
                $this->lang);

            $entity->setPaymentSessionId($paymentSessionId);
        }
    }
} 