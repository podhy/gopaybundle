<?php

namespace Podhy\GopayBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PodhyGopayBundle extends Bundle
{
    public function boot()
    {
        if ($this->container->getParameter('podhy.gopay.test') === true)
        {
            \GopayConfig::init(\GopayConfig::TEST);
        }
        else
        {
            \GopayConfig::init(\GopayConfig::PROD);
        }
    }

}
